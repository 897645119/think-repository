<?php
/**
 * User: 假如_丶 <897645119@qq.com>
 * Date: 2020/5/9
 * Time: 8:41
 * 无可奈何花落去，似曾相识燕归来。
 */


namespace fanxd\repository;

use fanxd\repository\command\repository\Create as RepositoryCreate;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands([
            RepositoryCreate::class
        ]);
    }
}
