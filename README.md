# think-repository

#### 介绍
`think-repository` 是为 `thinkphp 6.0.*` 提供的存储库用于抽象数据层，使我们的应用程序更灵活地进行维护。

#### ThinkPHP
`>= thinkphp 6.0.*`

#### 安装教程

##### Composer

> composer require fanxd/think-repository dev-master

#### 使用说明
>最好`在多应用`下使用 
##### 命令

>php think fanxd:repository Post

#### 路由

> Route::resource('post', 'PostController');

#### 可用的方法
- first($id) // 查找单条记录
- get() // 查找记录
- paginate() // 分页查询
- create($data) // 写入数据
- save($data) // 保存当前数据对象
- delete($where) // 删除记录
- update($where,$data) // 更新记录
- find($id) // 查找单条记录 如果不存在则抛出异常
- findWhere($where,$columns = ['*']) // 指定AND查询条件 查找单条记录
- with([]) // 关联查询
- search([]) // 数据搜索
- order($order) // 排序

#### 查找记录

>$posts=$this->repository->get();


#### 分页查询

>$posts=$this->repository->paginate($limit);

#### 按结果按id查找

>$posts=$this->repository->find($id);
>
>$posts=$this->repository->first($id);

#### 加载模型关系

>$posts=$this->repository->with(['state'])->find($id);

#### 按结果按字段名查找

>$posts=$this->repository->findByField('title', 'Hello');

#### 按结果由多个字段查找

>$posts=$this->repository->findWhere([
    'id' => 1
], ['id', 'title]);
>
#### 按结果在一个字段中查找多个值

>$posts=$this->repository->findWhereIn('id', [1,2,3,4,5]);

#### 通过排除一个字段中的多个值，按结果查找

>$posts=$this->repository->findWhereNotIn('id', [6,7,8,9,10]);

#### 写入数据

$post = $this->repository->create($data);

#### 更新记录

>$posts=$this->repository->update($where, $data);

#### 删除记录

>$this->repository->delete($id)


#### 按多个字段删除存储库中的条目

>$this->repository->deleteWhere([
    'id' => 1,
    'user_id' => 1
])

### Transformer
系统会自动生成`transform`文件,可自行选择是否启用,主要功能对我来说就是美化接口让我们更专业 :)

```
<?php

namespace app\api\transform;

use fanxd\repository\command\transform\Transform;

class PostTransform extends Transform
{
    public function transform($items)
    {
        return [
            'id'            => $items['id'],
            //... 
            'createTime'    => $items['create_time'],
            'updateTime'    => $items['update_time']
        ];
    }
}

```

### 参与贡献
[<img src="https://portrait.gitee.com/uploads/avatars/user/407/1221892_897645119_1578946668.png!avatar30">](https://gitee.com/897645119)

